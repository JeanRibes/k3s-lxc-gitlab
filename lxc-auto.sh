#!/usr/bin/env bash
set -x
lxc delete k3s-lxc --force
lxc launch images:debian/10 k3s-lxc
lxc config set k3s-lxc linux.kernel_modules "ip_tables,ip6_tables,netlink_diag,nf_nat,overlay"
lxc config set k3s-lxc raw.lxc "lxc.mount.auto=proc:rw sys:rw cgroup:rw"
lxc config set k3s-lxc security.nesting  "true"
lxc config set k3s-lxc security.privileged "true"
wget https://github.com/rancher/k3s/releases/download/v0.9.1/k3s
chmod +x k3s

lxc file push k3s k3s-lxc/usr/local/bin/k3s
lxc exec k3s-lxc -- /bin/sh -c "apt-get update; apt-get install -y ca-certificates; echo 'L /dev/kmsg - - - - /dev/console' > /etc/tmpfiles.d/kmsg.conf"
lxc exec k3s-lxc -- /bin/sh -c "echo 'L /dev/kmsg - - - - /dev/console' > /etc/tmpfiles.d/kmsg.conf"
lxc exec k3s-lxc -- /bin/sh -c "echo '[Unit]
Description=Lightweight Kubernetes
Documentation=https://k3s.io
After=network-online.target

[Service]
Type=notify
ExecStart=/usr/local/bin/k3s server
KillMode=process
Delegate=yes
LimitNOFILE=infinity
LimitNPROC=infinity
LimitCORE=infinity
TasksMax=infinity
TimeoutStartSec=0
Restart=always
RestartSec=5s

[Install]
WantedBy=multi-user.target'>/etc/systemd/system/k3s.service"
lxc exec k3s-lxc -- /bin/sh -c "systemctl enable k3s"
lxc restart k3s-lxc
lxc config device add k3s-lxc kube-api proxy listen=tcp:0.0.0.0:6443 connect=tcp:localhost:6443
lxc config device add k3s-lxc http proxy listen=tcp:0.0.0.0:80 connect=tcp:localhost:80
lxc config device add k3s-lxc https proxy listen=tcp:0.0.0.0:443 connect=tcp:localhost:443
sleep 30
lxc file pull k3s-lxc/var/lib/rancher/k3s/agent/kubeconfig.yaml ~/.kube/config