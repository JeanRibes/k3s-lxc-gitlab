#!/usr/bin/env bash
echo -n "IP: "
kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}'
echo "CA (pem):"
kubectl get secret $( kubectl get secrets|grep "kubernetes.io/service-account-token"|grep default-token|awk '{print $1}') -o jsonpath="{['data']['ca\.crt']}" | base64 --decode

echo "apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab-admin
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: gitlab-admin
  namespace: kube-system
" > gitlab-admin-service-account.yaml
kubectl apply -f gitlab-admin-service-account.yaml
echo -n "Token : "
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}')|grep "token:"|awk '{print $2}'
